Found a 17 line (109 tokens) duplication in the following files: 
Starting at line 86 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/view/gui/TopPanel.java
Starting at line 127 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/view/gui/TopPanel.java

                    view.getController().loadInitialState(simulationChooser.getSelectedFile().getPath());
                } catch (IOException ex) {
                    final JLabel message = new JLabel("An error occurred trying to load the simulation");
                    message.setFont(GuiUtils.FONT);
                    JOptionPane.showMessageDialog(this, message);
                } catch (IllegalExtensionException ex) {
                    final JLabel message = new JLabel("The extension of the file was not correct");
                    message.setFont(GuiUtils.FONT);
                    JOptionPane.showMessageDialog(this, message);
                } catch (FileFormatException ex) {
                    final JLabel message = new JLabel(
                            "The selected file does not contain a replay, has been corrupted or use an outdated format");
                    message.setFont(GuiUtils.FONT);
                    JOptionPane.showMessageDialog(this, message);
                }
            }
        });
=====================================================================
Found a 23 line (103 tokens) duplication in the following files: 
Starting at line 24 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/model/state/PositionImpl.java
Starting at line 24 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/view/model/ViewPositionImpl.java

    public PositionImpl(final double x, final double y) {
        this.coordinates = new Pair<>(x, y);
    }

    @Override
    public double getX() {
        return this.coordinates.getFirst();
    }

    @Override
    public double getY() {
        return this.coordinates.getSecond();
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            final PositionImpl other = (PositionImpl) obj;
=====================================================================
Found a 12 line (92 tokens) duplication in the following files: 
Starting at line 63 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/model/food/FoodImpl.java
Starting at line 70 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/view/model/food/CreationViewFoodImpl.java

            final FoodImpl other = (FoodImpl) obj;
            return Objects.equals(this.name, other.name)
                    && Objects.equals(this.nutrients.keySet(), other.nutrients.keySet())
                    && this.nutrients.keySet().stream()
                            .allMatch(k -> other.nutrients.get(k).doubleValue() == this.nutrients.get(k).doubleValue());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Food's Name=" + name + ", nutrients=" + nutrients;
=====================================================================
Found a 19 line (68 tokens) duplication in the following files: 
Starting at line 129 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/model/simulator/bacteria/ActionPerformerImpl.java
Starting at line 208 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/model/simulator/bacteria/ActionPerformerImpl.java

                }
            } catch (NotEnoughEnergyException e) {
                bacteria.spendEnergy(bacteria.getEnergy());
            } finally {
                // Logger.getInstance().info("MOVE" + this.bactEnv.getQuad(bacteriaPos),
                // "THREAD" + Thread.currentThread().getId() + " OUT");
                if (isSafe) {
                    this.releaseEnvMutex(this.bactEnv.getQuadrant(bacteriaPos));
                } else {
                    this.releaseEnvMutex(SINGLE_MUTEX_INDEX);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void eat(final Position bacteriaPos, final Bacteria bacteria, final Optional<Position> foodPosition,
=====================================================================
Found a 22 line (57 tokens) duplication in the following files: 
Starting at line 125 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/model/simulator/EnvironmentUtil.java
Starting at line 151 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/model/simulator/EnvironmentUtil.java

        }).map(position -> (Position) position).distinct().filter(position -> position.getX() < maxPosition.getX()
                && position.getX() > 0 && position.getY() < maxPosition.getY() && position.getY() > 0);
    }

    /**
     * Generate stream of Position in the range [(startX, startY), (endX, endY)].
     * 
     * @param startX
     *            the start value for the X coordinate
     * @param endX
     *            the end value for the X coordinate
     * @param startY
     *            the start value for the Y coordinate
     * @param endY
     *            the end value for the Y coordinate
     * @param bacteriaPos
     *            the original Position of the Bacteria
     * @param maxPosition
     *            the maxiumPosition representing a limit for each position
     * @return a stream of Position in the given range excluding the same Position
     */
    public static Stream<Position> positionStream(final int startX, final int endX, final int startY, final int endY,
=====================================================================
Found a 2 line (55 tokens) duplication in the following files: 
Starting at line 55 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/view/gui/SimulationPanel.java
Starting at line 62 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/view/gui/SimulationPanel.java

                g.fillRect((int) e.getKey().getX() - radius.getXRadius(), (int) e.getKey().getY() - radius.getYRadius(),
                        2 * radius.getXRadius(), 2 * radius.getYRadius());
=====================================================================
Found a 4 line (53 tokens) duplication in the following files: 
Starting at line 67 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/tests/TestNutrientStorage.java
Starting at line 73 of /home/danysk/testworkspace/oop17-barbaro-simone-mengozzi-federico-semprini-gloria-ziani/src/tests/TestNutrientStorage.java

        assertFalse("The storage should be empty", storage.getNutrients().isEmpty());
        assertEquals("The storage should not have energy", 0, storage.getEnergyStored().getAmount(),
                TestUtils.getDoubleCompareDelta());
        assertThrows(NotEnoughEnergyException.class, () -> storage.takeEnergy(new EnergyImpl(1)));
